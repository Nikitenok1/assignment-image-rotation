#include "image_transformer.h"

typedef enum transform_status(image_transformer)(struct image const source, struct image* result);

static image_transformer *image_transformers[IMAGE_TRANSFORMER_COUNT] = {
        [ROTATE_CCV_90] = rotate_ccv_90
};

enum transform_status transform_image(struct image const source, struct image* result,  enum image_transformer_code it_code) {
    return image_transformers[it_code](source, result);
}
