#include "image.h"

struct image create_image(uint64_t width, uint64_t height) {
    return (struct image) {.width = width, .height = height, .data = malloc(height * width * sizeof(struct pixel))};
}

void free_image(struct image *used) {
    if (used != NULL) free(used->data);
}

void set_pixel(struct image* img, uint64_t x, uint64_t y, struct pixel pixel) {
    if (img != NULL) img -> data[(img -> width) * y + x] = pixel;
}
