#ifndef BMP_SERIALIZER
#define BMP_SERIALIZER

#include <inttypes.h>

#include "bmp_header.h"
#include "image.h"
#include "image_io_status.h"

#define BF_TYPE 0x4D42
#define BI_SIZE 40
#define BI_PLANES 1
#define BI_COMPRESSION 0
#define BF_RESERVED 0
#define BI_BIT_COUNT 24
#define BI_X_PELS_PER_METER 2834
#define BI_Y_PELS_PER_METER 2834
#define BI_CLR_USED 0
#define BI_CLR_IMPORTANT 0

enum read_status bmp_to_image(FILE* in, struct image* img);
enum write_status image_to_bmp(FILE *out, struct image const *img);

#endif
