#ifndef BMP_ROTATE
#define BMP_ROTATE

#include "image.h"
#include "image_io_status.h"

enum transform_status rotate_ccv_90(struct image const source, struct image* result);

#endif
