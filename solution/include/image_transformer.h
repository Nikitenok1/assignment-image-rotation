#ifndef IMAGE_TRANSFORMER
#define IMAGE_TRANSFORMER

#include "rotate.h"
#include "image_io_status.h"

#define IMAGE_TRANSFORMER_COUNT 1

enum image_transformer_code {
    ROTATE_CCV_90 = 0  // ccv - counterclockwise
};

enum transform_status transform_image(struct image const source, struct image* result,  enum image_transformer_code it_code);

#endif
