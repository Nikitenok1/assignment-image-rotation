#ifndef IMAGE
#define IMAGE

#include <stdint.h>
#include <stdbool.h>
#include <malloc.h>

struct pixel { uint8_t b, g, r; };
struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image create_image(uint64_t width, uint64_t height);
void free_image(struct image *used);
void set_pixel(struct image* img, uint64_t x, uint64_t y, struct pixel pixel);

#endif
